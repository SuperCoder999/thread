import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';

import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment
} from 'src/containers/Thread/actions';

import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

import {
  likeComment,
  dislikeComment,
  initComments
} from './actions';

const ExpandedPost = ({
  post,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  likeComment: likeC,
  dislikeComment: dislikeC,
  addComment: add,
  initComments: initC
}) => {
  initC();

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              likePost={like}
              dislikePost={dislike}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    like={likeC}
                    dislike={dislikeC}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  initComments: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: {
    ...rootState.posts.expandedPost,
    ...(rootState.comments.comments.filter(c => c.postId === rootState.posts.expandedPost.id))
  }
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  likeComment,
  dislikeComment,
  initComments
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
