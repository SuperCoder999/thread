import { SET_COMMENTS } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_COMMENTS:
      return {
        ...state,
        comments: action.comments
      };
    default:
      return state;
  }
};
