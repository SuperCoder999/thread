import {
  likeComment as requestLikeComment,
  dislikeComment as requestDislikeComment,
  getAllComments
} from 'src/services/commentService';

import { SET_COMMENTS } from './actionTypes';

const setCommentsAction = comments => ({
  type: SET_COMMENTS,
  comments
});

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { id } = await requestLikeComment(commentId);
  const diff = id ? 1 : -1;

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff
  });

  const { comments: { comments } } = getRootState();
  const updated = comments.map(comment => (comment.id === commentId ? comment : mapLikes(comment)));

  dispatch(setCommentsAction(updated));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { id } = await requestDislikeComment(commentId);
  const diff = id ? 1 : -1;

  const mapLikes = comment => ({
    ...comment,
    dislikeCount: Number(comment.dislikeCount) + diff
  });

  const { comments: { comments } } = getRootState();
  const updated = comments.map(comment => (comment.id === commentId ? comment : mapLikes(comment)));

  dispatch(setCommentsAction(updated));
};

export const initComments = () => async dispatch => {
  const comments = await getAllComments();
  dispatch(setCommentsAction(comments));
};
