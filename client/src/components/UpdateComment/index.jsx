import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form } from 'semantic-ui-react';

const UpdateComment = ({
  id,
  defaultBody,
  updComment
}) => {
  const [body, setBody] = useState('');

  const handleUpdComment = async () => {
    if (!body) {
      return;
    }
    await updComment(id, { body });
    setBody('');
    window.location.reload();
  };

  return (
    <Form onSubmit={handleUpdComment}>
      <Form.TextArea
        style={{ height: '100px' }}
        name="body"
        placeholder="Type a new comment!"
        onChange={ev => setBody(ev.target.value)}
        defaultValue={defaultBody}
      />
      <Button floated="right" color="blue" type="submit">Update</Button>
    </Form>
  );
};

UpdateComment.propTypes = {
  id: PropTypes.string.isRequired,
  defaultBody: PropTypes.string.isRequired,
  updComment: PropTypes.func.isRequired
};

export default UpdateComment;
