import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import moment from 'moment';
import UpdatePost from '../UpdatePost/index';
import DeletePost from '../DeletePost/index';
import { updPost, delPost } from '../../services/postService';

import styles from './styles.module.scss';

const update = (id, req) => { updPost(id, req); window.location.reload(); };

const del = id => { delPost(id); window.location.reload(); };

const Post = ({ post, likePost, dislikePost, toggleExpandedPost, sharePost, userId }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt
  } = post;
  const date = moment(createdAt).fromNow();
  const dateUPD = moment(updatedAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Meta>
          <span className="date">
            updated
            {' '}
            {dateUPD}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {user.id === userId
          ? (
            <>
              <Label style={{ width: '100%' }}>
                <UpdatePost
                  id={id}
                  defaultBody={body}
                  defaultImage={image}
                  updatePost={update}
                  uploadImage={imageService.uploadImage}
                />
                <DeletePost id={id} delPost={del} />
              </Label>
            </>
          )
          : ''}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

export default Post;
