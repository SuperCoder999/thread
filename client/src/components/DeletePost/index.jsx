import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form } from 'semantic-ui-react';

const DeletePost = ({
  id,
  delPost
}) => {
  const handleDelPost = async () => {
    await delPost(id);
  };

  return (
    <Form onSubmit={handleDelPost}>
      <Button color="red" floated="right" type="submit">Delete</Button>
    </Form>
  );
};

DeletePost.propTypes = {
  id: PropTypes.string.isRequired,
  delPost: PropTypes.func.isRequired
};

export default DeletePost;
