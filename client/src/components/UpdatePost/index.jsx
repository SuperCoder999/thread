import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Segment, Form, Image, Icon } from 'semantic-ui-react';

import styles from './styles.module.scss';

const UpdatePost = ({
  id,
  defaultBody,
  defaultImage,
  updatePost,
  uploadImage
}) => {
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdPost = async () => {
    if (!body) {
      return;
    }
    await updatePost(id, { imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleUpdPost}>
        <Form.TextArea
          style={{ height: '150px' }}
          name="body"
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
          defaultBody={defaultBody}
        />
        {defaultImage?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={defaultImage?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">Update</Button>
      </Form>
    </Segment>
  );
};

UpdatePost.propTypes = {
  id: PropTypes.string.isRequired,
  defaultBody: PropTypes.string.isRequired,
  defaultImage: PropTypes.objectOf(PropTypes.any),
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

UpdatePost.defaultProps = {
  defaultImage: null
};

export default UpdatePost;
