import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { updateComment, deleteComment } from 'src/services/commentService';
import { getCurrentUser } from 'src/services/authService';
import UpdateComment from '../UpdateComment/index';
import DeleteComment from '../DeleteComment/index';
import Spinner from '../Spinner/index';

import styles from './styles.module.scss';

async function isAuthorOfComment(user) {
  const profile = await getCurrentUser();
  return profile.id === user.id;
}

class Comment extends Component {
  constructor({
    comment: {
      id,
      body,
      createdAt,
      updatedAt,
      deletedAt,
      likeCount = 0,
      dislikeCount = 0,
      user
    },
    like,
    dislike
  }) {
    super();
    this.id = id;
    this.body = body;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
    this.user = user;
    this.likeCount = likeCount;
    this.dislikeCount = dislikeCount;
    this.like = like;
    this.dislike = dislike;
    this.state = { isAuthor: undefined };
  }

  componentDidMount() {
    isAuthorOfComment(this.user).then(res => {
      this.setState({
        isAuthor: res
      });
    });
  }

  render() {
    const { isAuthor } = this.state;

    if (!this.deletedAt) {
      if (isAuthor !== undefined) {
        return (
          <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(this.user.image)} />
            <CommentUI.Content>
              <CommentUI.Author as="a">
                {this.user.username}
              </CommentUI.Author>
              <CommentUI.Metadata>
                created
                {' '}
                {moment(this.createdAt).fromNow()}
              </CommentUI.Metadata>
              <CommentUI.Metadata>
                updated
                {' '}
                {moment(this.updatedAt).fromNow()}
              </CommentUI.Metadata>
              <CommentUI.Text>
                {this.body}
              </CommentUI.Text>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => {
                  this.like(this.id);
                  let upd = this.forceUpdate;
                  upd = upd.bind(this);
                  setTimeout(upd, 1000);
                }}
              >
                <Icon name="thumbs up" />
                {this.likeCount}
              </Label>
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => this.dislike(this.id)}
              >
                <Icon name="thumbs down" />
                {this.dislikeCount}
              </Label>
              {isAuthor
                ? (
                  <Label style={{ width: '70%' }}>
                    <UpdateComment
                      id={this.id}
                      defaultBody={this.body}
                      updComment={updateComment}
                    />
                    <DeleteComment
                      id={this.id}
                      delComment={deleteComment}
                    />
                  </Label>
                )
                : ''}
            </CommentUI.Content>
          </CommentUI>
        );
      }

      return (
        <Spinner />
      );
    }

    return '';
  }
}

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  like: PropTypes.func.isRequired,
  dislike: PropTypes.func.isRequired
};

export default Comment;
