import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const DeleteComment = ({
  id,
  delComment
}) => {
  const handleDelComment = async () => {
    await delComment(id);
    window.location.reload();
  };

  return (
    <Form onSubmit={handleDelComment}>
      <Button floated="right" type="submit" color="red">Delete</Button>
    </Form>
  );
};

DeleteComment.propTypes = {
  id: PropTypes.string.isRequired,
  delComment: PropTypes.func.isRequired
};

export default DeleteComment;
