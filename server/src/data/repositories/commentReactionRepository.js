import BaseRepository from './baseRepository';
import { CommentReactionModel, CommentModel } from '../models/index';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
