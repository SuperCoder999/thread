import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentRepository extends BaseRepository {
  getComments() {
    return this.model.findAll({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: {
        deletedAt: null
      },
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [
        {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: CommentReactionModel,
          attributes: [],
          duplicating: false
        }
      ]
    });
  }

  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: {
        id,
        deletedAt: null
      },
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [
        {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: CommentReactionModel,
          attributes: []
        }
      ]
    });
  }

  async updateCommentById(id, data) {
    const result = await this.updateById(id, data);
    return result;
  }

  async deleteCommentById(id) {
    const result = await this.updateCommentById(id, { deletedAt: new Date() });
    return result;
  }
}

export default new CommentRepository(CommentModel);
