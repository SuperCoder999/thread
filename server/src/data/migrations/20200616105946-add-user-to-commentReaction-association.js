module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction(transaction => queryInterface.addColumn(
    'commentReactions',
    'userId',
    {
      type: Sequelize.UUID,
      references: {
        model: 'users',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
    { transaction }
  )),

  down: queryInterface => queryInterface.sequelize.transaction(transaction => queryInterface.removeColumn(
    'commentReactions',
    'userId',
    { transaction }
  ))
};
