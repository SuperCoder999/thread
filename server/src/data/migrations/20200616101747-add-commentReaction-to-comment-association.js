module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction(transaction => queryInterface.addColumn(
    'commentReactions',
    'commentId',
    {
      type: Sequelize.UUID,
      references: {
        model: 'comments',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
    { transaction }
  )),

  down: queryInterface => queryInterface.sequelize.transaction(transaction => queryInterface.removeColumn(
    'commentReactions',
    'commentId',
    { transaction }
  ))
};
