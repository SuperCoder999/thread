module.exports = { // eslint-disable-next-line arrow-body-style
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction(transaction => {
    return queryInterface.createTable('commentReactions', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('gen_random_uuid()')
      },
      isLike: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    }, { transaction });
  }),

  down: queryInterface => queryInterface.sequelize.transaction(transaction => queryInterface.dropTable(
    'commentReactions',
    { transaction }
  ))
};
