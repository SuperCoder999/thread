module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction(transaction => queryInterface.addColumn(
    'comments',
    'deletedAt',
    {
      allowNull: true,
      type: Sequelize.DATE
    },
    { transaction }
  )),

  down: queryInterface => queryInterface.sequelize.transaction(transaction => queryInterface.removeColumn(
    'comments',
    'deletedAt',
    { transaction }
  ))
};
