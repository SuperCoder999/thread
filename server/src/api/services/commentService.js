import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepositiory from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const updateCommentById = (id, data) => commentRepository.updateCommentById(id, data);

export const deleteCommentById = id => commentRepository.deleteCommentById(id);

export const getAllComments = () => commentRepository.getComments();

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // define the callback for future use as a promise: DONE
  const updateOrDelete = async react => {
    const result = (react.isLike === isLike
      ? await commentReactionRepositiory.deleteById(react.id)
      : await commentReactionRepositiory.updateById(react.id, { isLike }));
    return result;
  };

  const reaction = await commentReactionRepositiory.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepositiory.create({ userId, commentId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : commentReactionRepositiory.getCommentReaction(userId, commentId);
};
