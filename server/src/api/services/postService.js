import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const update = async (postId, data) => {
  const res = await postRepository.updatePostById(postId, data);
  return res;
};

export const deletePost = async postId => {
  const res = await postRepository.deletePostById(postId);
  return res;
};

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise: DONE
  const updateOrDelete = async react => {
    const result = (react.isLike === isLike
      ? await postReactionRepository.deleteById(react.id)
      : await postReactionRepository.updateById(react.id, { isLike }));
    return result;
  };

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
